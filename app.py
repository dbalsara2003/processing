import connexion
from connexion import NoContent
import datetime
import json
import logging
import logging.config
import requests
from requests.sessions import session
from sqlalchemy.engine import result
import yaml
import apscheduler
from apscheduler.schedulers.background import BackgroundScheduler

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from stats import Stats

DB_ENGINE = create_engine("sqlite:///stats.sqlite")
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

def get_latest_stats():
    with DB_SESSION.begin() as session:
        latest_stat = session.query(Stats).order_by(Stats.last_updated).first() 
        if latest_stat:
            return latest_stat.to_dict(), 200
    return NoContent, 201

def populate_stats():
    timestamp = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    last_updated = timestamp

    logger.info('Assignment 3.1')
    logger.info('Processing started')

    with DB_SESSION.begin() as session:
        result = session.query(Stats).order_by(Stats.last_updated.desc()).first()
        if result:
            result.to_dict()
            last_updated = result.last_updated

    resBuy = requests.get(f'http://localhost:8090/buy?timestamp={last_updated}')
    resBuy = resBuy.json()

    total_buy_price = 0
    total_num_buys = 0

    for items in resBuy:
        if items['item_price'] > total_buy_price:
            total_buy_price = items['item_price']
        total_num_buys += items['item_price']

    resSell = requests.get(f'http://localhost:8090/buy?timestamp={last_updated}')

    resSell = resSell.json()

    total_sell_price = 0
    total_num_sells = 0

    for items in resSell:
        if items['item_price'] > total_sell_price:
            total_sell_price = items['item_price']
        total_num_sells += items['item_price']

    UpdatedStats = Stats(total_buy_price, total_num_buys, total_sell_price, total_num_sells, timestamp)

    session.add(UpdatedStats)
    session.commit()
    session.close()


    return NoContent, 201

def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats, 'interval', seconds=app_config['period'])
    sched.start()

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml", strict_validation=True, validate_responses=True)

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basic')

if __name__ == "__main__":
    init_scheduler()
    app.run(port=8100, use_reloader=False)
